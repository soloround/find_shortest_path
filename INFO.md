### Shortest path algorithm:
The algorithm works as follows:
##### Building the Graph:
There's an infinite amount of possible vertices and edges on the canvas,
because the grid is continuous . Therefore, before trying to find the shortest path, the grid needs to be reduced to a sub-set of "interesting" vertices and edges.
* Initialize a set of vertices `discovered_v` which will hold the `source` and `target` vertices.
* Initialize a set of edges `discovered_e`.
* For each two vertices in the set `discovered_v` do the following:
  + Set vertices as `source` and `target`.
  + Draw an edge between `source` and `target` and find all the intersections of that edge with the polygons.
    There can be two types of intersections with a polygon:
      - Two point intersection - means that the edge has entered and exited the polygon. In this case, pick the closest                         intersection line of the two, `line_int`, and retrieve line's two bounding points `point_1` and `point_2`.
      - One point intersection - means that the edge and polygon share a vertex. From that vertex, the line runs inside the polygon and         exits with one intersection. In this case, if the shared vertex is closer than the intersection point, retrieve two                     points, `point_1` and `point_2`, which are adjacent to the vertex on the polygon. If not, handle the intersection line as               explained above.
  + If intersections were found, add them to `discovered_v`.
  + If not, it means that the path from `source` to `target` isn't obstructed by a polygon. 
    There is also a possibility that the line is fully contained inside a polygon and is, in fact, one of the polygon's diagonals.
    If the line isn't a diagonal, then add two directed edges, from `source` to `target` and from `target` to `source`, to `discovered_e`.
* The loop stops when no new vertices are discovered and added to `discovered_v`.

![option A](http://i.imgur.com/rv40nFH.png)

![option B](http://i.imgur.com/8ma4hcX.png)

##### Running the search algorithm:
After discovering the vertices and edges, finding the shortest path is much easier. 
I picked `Dijkstra Algorithm`, which unlike `Breadth-First Search`, works with weighted graphs.
The algorithm's time complexity is `O((V + E)logV)` when it uses a min-heap.

### Design:
The project consists of two parts:

#### The server:
Serves all the static files, pages and also the api for finding the shortest path.
* Vertex, Edge and Polygon are represented as objects with their own classes for ease of use.
  All the functions that are related to these objects are stored within their classes.
* BuildGraph discovers the relevant vertices and edges.
* Dijkstra runs dijkstra algorithm on discovered graph.
* FindShortestPath utilizes the objects above to calculate the shortest path.
* server.py is the main class. It handles all the REST requests from the client.

#### The front-end:
The app was written with ES6 and modules aren't supported by most of the browsers,
so I had to use `webpack` to make the js code compatible with all browsers.
Webpack compiles two files `app.bundle.js` and `vendor.bundle.js`, which reside in `server/static/js/`
and are injected to `server/templates/index.tmpl.pre-gulp.html` which is outputed as `index.tmpl.html`.

The app has two services and three components.
##### Components:
The page was seperated into 3 components:
* Stage holds the canvas and handles user's clicks on the canvas. 
  Most of the business logic resides in the two services that work with this component.
  Upon receiving a click request, it determines what should be added - source, target or obstacle,
  and then sends the point to LogicService, so that it will be added to the state.
  After updating the logic, it draws the new item using the DrawService.
* Messaging explains the user what should he do next, according to the state of the app.
  It communicates with the LogicService in order to get the current state.
* Operations lets the user to reset the canvas or send the canvas to find the shortest path. 
  The user can send the canvas only after the user entered all the necessary data (source, target, obstacles - if any).
  The send operation receives the state from the LogicService and sends it to the server.
  After receiving the path, it draws it using the DrawService.
##### Services:
The components use the following services:
* Draw is in charge of drawing and deleting the objects inside the canvas.
* Logic holds the state which consists the source and target points, all the obstacles and the obstacle which the user is currently working on.
  It also does all the logic for creating an obstacle and the current step of the app.
##### Libraries:
* EaselJS is used for making the canvas interactive.
* Bootstrap + JQuery is used for the design.
* AngularJS.


### Assumptions:
- The polygons don't intersect each other.
- All polygons are convex.
- Source & target points aren't placed inside any of the polygons.
