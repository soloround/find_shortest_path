'use strict';

import gulp     from 'gulp';
import webpack  from 'webpack';
import path     from 'path';
import gutil    from 'gulp-util';
import webpackDevMiddelware from 'webpack-dev-middleware';
import webpachHotMiddelware from 'webpack-hot-middleware';
import colorsSupported      from 'supports-color';

let root = 'server/static';

// Helper method for resolving paths
let resolveToApp = (glob = '') => {
  return path.join(root, 'js/app', glob); 
};

let resolveToComponents = (glob = '') => {
  return path.join(root, 'js/app/components', glob); 
};

// Map of all paths
let paths = {
  js: resolveToComponents('**/*.js'), 
  css: resolveToApp('**/*.css'),
  html: [
    resolveToApp('**/*.html'),
    path.join(root, 'index.html')
  ],
  entry: path.join(__dirname, root, 'js/app/app.js'),
  output: root
};
// Build modules.
gulp.task('webpack', (cb) => {
  const config = require('./webpack.dev.config');
  config.entry.app = paths.entry;

  webpack(config, (err, stats) => {
    if(err)  {
      throw new gutil.PluginError("webpack", err);
    }

    gutil.log("[webpack]", stats.toString({
      colors: colorsSupported,
      chunks: false,
      errorDetails: true
    }));

    cb();
  });
});
gulp.task('watch', ['webpack']);
gulp.task('default', ['webpack']);
