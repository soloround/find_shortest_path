# Find Shortest Path

![example](http://i.imgur.com/vgevKv4.png)

Implemented with `Python 2.7.10` & `Flask 0.12.2`.

Front end was written with `Angular 1.5.2` and `ES6`.

### Prerequisites:
Install `Flask 0.12.2`.

### Run:
To run the project, simply execute
`python server.py`.

The server will start listening at http://localhost:8001

### Notes:
- Webpack was used to support browsers that can't run ES6 modules.
  To run the webpack process, first install the required modules with
  `npm install`
  and then run
  `gulp webpack`.
- The file `INFO.md` contains more information about the design and the algorithm that I picked to find the shortest path.

### More Info:
You can read a detailed explanation about the algorithm that I used and this solution's design [here](INFO.md)

