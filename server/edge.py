from vertex import Vertex


class Edge(object):
    def __init__(self, vertex_a, vertex_b):
        self.vertex_from = vertex_a
        self.vertex_to = vertex_b

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
            self.vertex_from == other.vertex_from and self.vertex_to == other.vertex_to

    def __hash__(self):
        return hash((self.vertex_from, self.vertex_to))

    @classmethod
    def from_vertices_json(cls, vertex_a_json, vertex_b_json):
        vertex_a = Vertex.from_json(vertex_a_json)
        vertex_b = Vertex.from_json(vertex_b_json)
        return cls(vertex_a, vertex_b)

    @classmethod
    def from_vertices(cls, vertex_a, vertex_b):
        return cls(vertex_a, vertex_b)

    @classmethod
    def from_json(cls, edge_json):
        vertex_a_json = Vertex.from_json({
            'x': edge_json['x1'],
            'y': edge_json['y1']
        })
        vertex_b_json = Vertex.from_json({
            'x': edge_json['x2'],
            'y': edge_json['y2']
        })
        return cls(vertex_a_json, vertex_b_json)

    def swap_orientation(self):
        return type(self)(self.vertex_to, self.vertex_from)

    def get_vertices(self):
        return self.vertex_from, self.vertex_to

    def has_vertex(self, vertex):
        """Checks if the given vertex is one of the edge's vertices."""
        if vertex != self.vertex_from and vertex != self.vertex_to:
            return False
        else:
            return True

    def calc_len(self):
        return self.vertex_from.calc_dist(self.vertex_to)

    def bounds_vertex(self, vertex):
        """Checks if the given vertex is bounded by the vertices of the edge."""
        return (self.vertex_from.y <= vertex.y <= self.vertex_to.y or self.vertex_to.y <= vertex.y <= self.vertex_from.y) and \
               (self.vertex_from.x <= vertex.x <= self.vertex_to.x or self.vertex_to.x <= vertex.x <= self.vertex_from.x)

    def get_coefs(self):
        """Calculates coefficients for the general equation Ax + by + C = 0.
        See following for equation:
        http://zonalandeducation.com/mmts/functionInstitute/edgearFunctions/newLgf/lgf0.php
        """
        a = self.vertex_from.y - self.vertex_to.y
        b = self.vertex_to.x - self.vertex_from.x
        c = self.vertex_from.x * self.vertex_to.y - \
            self.vertex_to.x * self.vertex_from.y
        return {'A': a, 'B': b, 'C': -c}

    def intersect_with_edge(self, other_edge):
        """Calculates the intersection of two edges.
        See following for equation:
        https://math.stackexchange.com/questions/424723/determinant-in-edge-edge-intersection
        """
        this_edge_coefs = self.get_coefs()
        other_edge_coefs = other_edge.get_coefs()
        det = this_edge_coefs['A'] * other_edge_coefs['B'] - \
              this_edge_coefs['B'] * other_edge_coefs['A']
        det_x = this_edge_coefs['C'] * other_edge_coefs['B'] - \
                this_edge_coefs['B'] * other_edge_coefs['C']
        det_y = this_edge_coefs['A'] * other_edge_coefs['C'] - \
                this_edge_coefs['C'] * other_edge_coefs['A']
        if det != 0:
            vertex = Vertex(float(det_x) / det, float(det_y) / det)
            # Touching a vertex doesn't count as an intersection.
            if self.has_vertex(vertex) or other_edge.has_vertex(vertex):
                return None
            # Vertex must be bounded by both edges.
            if self.bounds_vertex(vertex) and other_edge.bounds_vertex(vertex):
                return vertex
            else:
                return None
        else:
            return None

    def to_json(self):
        return {
            'x1': self.vertex_from.x,
            'y1': self.vertex_from.y,
            'x2': self.vertex_to.x,
            'y2': self.vertex_to.y
        }
