webpackJsonp([0],[
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(1);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _angularUiRouter = __webpack_require__(3);
	
	var _angularUiRouter2 = _interopRequireDefault(_angularUiRouter);
	
	var _appComponent = __webpack_require__(4);
	
	var _appComponent2 = _interopRequireDefault(_appComponent);
	
	var _servicesLogicServiceLogicService = __webpack_require__(6);
	
	var _servicesLogicServiceLogicService2 = _interopRequireDefault(_servicesLogicServiceLogicService);
	
	var _servicesDrawServiceDrawService = __webpack_require__(7);
	
	var _servicesDrawServiceDrawService2 = _interopRequireDefault(_servicesDrawServiceDrawService);
	
	var _componentsStageStage = __webpack_require__(8);
	
	var _componentsStageStage2 = _interopRequireDefault(_componentsStageStage);
	
	var _componentsOperationsOperations = __webpack_require__(12);
	
	var _componentsOperationsOperations2 = _interopRequireDefault(_componentsOperationsOperations);
	
	var _componentsMessagingMessaging = __webpack_require__(16);
	
	var _componentsMessagingMessaging2 = _interopRequireDefault(_componentsMessagingMessaging);
	
	_angular2['default'].module('app', [_angularUiRouter2['default'], _componentsStageStage2['default'].name, _componentsOperationsOperations2['default'].name, _componentsMessagingMessaging2['default'].name]).config(["$locationProvider", "$stateProvider", "$urlRouterProvider", function ($locationProvider, $stateProvider, $urlRouterProvider) {
	    "ngInject";
	
	    $stateProvider.state('app', {
	        url: '/app',
	        template: '<app></app>'
	    });
	    $urlRouterProvider.otherwise('/app');
	}]).service('LogicService', _servicesLogicServiceLogicService2['default']).service('DrawService', _servicesDrawServiceDrawService2['default']).component('app', _appComponent2['default']);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _appTmplHtml = __webpack_require__(5);
	
	var _appTmplHtml2 = _interopRequireDefault(_appTmplHtml);
	
	var appComponent = {
	  template: _appTmplHtml2['default'],
	  restrict: 'E'
	};
	
	exports['default'] = appComponent;
	module.exports = exports['default'];

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = "<div class=\"container\">\n  <h1 class=\"text-center center-block\">Find Shortest Path for Boat</h1>\n  <div class=\"row\">\n      <div class=\"col-md-12\" style=\"min-height: 30px\"></div>\n      <div class=\"col-md-12\">\n          <messaging></messaging>\n      </div>\n      <div class=\"col-md-12\" style=\"min-height: 30px\"></div>\n      <div class=\"col-md-12\">\n          <stage class=\"center-block\"></stage>\n      </div>\n      <div class=\"col-md-12\" style=\"min-height: 30px\"></div>\n      <div class=\"col-md-12\">\n          <operations></operations>\n      </div>\n  </div>\n</div>"

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	// $timeout is used to invoke the digest cycle,
	// so that the view will update.
	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }
	
	function logicService($timeout) {
	    "ngInject";
	
	    // Radius for determining if two points
	    // are too close to each other.
	    var radius = 10;
	    // Holds current state.
	    var state = {
	        isSent: false,
	        source: undefined,
	        target: undefined,
	        currObstacle: [],
	        obstacles: []
	    };
	    /*
	    Used to lock the polygon and to avoid picking points
	    that are too clock to each other.
	     */
	    function isNearRadius(pointA, pointB) {
	        // If point B is within the radius of point A.
	        return pointA['x'] - radius <= pointB['x'] && pointB['x'] <= pointA['x'] + radius && pointA['y'] - radius <= pointB['y'] && pointB['y'] <= pointA['y'] + radius;
	    }
	    // Build a polygon from a list of points.
	    function convertObstacleToPolygon() {
	        var polygon = [];
	        for (var i = 1; i < state.currObstacle.length; i++) {
	            polygon.push(convertPointsToLine(state.currObstacle[i - 1], state.currObstacle[i]));
	        }
	        return polygon;
	    }
	    // Converts two points to a line.
	    function convertPointsToLine(pointA, pointB) {
	        return {
	            'x1': pointA['x'],
	            'y1': pointA['y'],
	            'x2': pointB['x'],
	            'y2': pointB['y']
	        };
	    }
	    return {
	        addObstacle: function addObstacle(point) {
	            // First point of the obstacle.
	            if (state.currObstacle.length === 0) {
	                $timeout(function () {
	                    state.currObstacle.push(point);
	                }, 0);
	                return {
	                    'type': 'point',
	                    'data': point
	                };
	            } else {
	                var _ret = (function () {
	                    var firstPoint = state.currObstacle[0];
	                    var lastPoint = [].concat(_toConsumableArray(state.currObstacle)).pop();
	                    /*
	                    The polygon consists of at least two points
	                    and the last point is pretty close to the first point
	                    of the polygon.
	                    In this case the polygon is complete.
	                    */
	                    if (state.currObstacle.length > 1 && isNearRadius(firstPoint, point)) {
	                        var polygonPoints = [].concat(_toConsumableArray(state.currObstacle));
	                        polygonPoints.push(firstPoint);
	                        $timeout(function () {
	                            state.currObstacle.push(firstPoint);
	                            var polygonLines = convertObstacleToPolygon();
	                            state.currObstacle = [];
	                            state.obstacles.push(polygonLines);
	                        }, 0);
	                        return {
	                            v: {
	                                'type': 'line',
	                                'data': convertPointsToLine(lastPoint, firstPoint),
	                                'shape': polygonPoints,
	                                'hasEnded': true
	                            }
	                        };
	                    }
	                    // The new point isn't too close to the last point entered.
	                    else if (!isNearRadius(lastPoint, point)) {
	                            $timeout(function () {
	                                state.currObstacle.push(point);
	                            }, 0);
	                            return {
	                                v: {
	                                    'type': 'line',
	                                    'data': convertPointsToLine(lastPoint, point),
	                                    'hasEnded': false
	                                }
	                            };
	                        }
	                        /*
	                        The new point was too close to the last point entered,
	                        so it is ignored.
	                         */
	                        else {
	                                return {
	                                    v: undefined
	                                };
	                            }
	                })();
	
	                if (typeof _ret === 'object') return _ret.v;
	            }
	        },
	        setSource: function setSource(newSource) {
	            $timeout(function () {
	                state.source = newSource;
	            }, 0);
	        },
	        getSource: function getSource() {
	            return state.source;
	        },
	        setTarget: function setTarget(newTarget) {
	            $timeout(function () {
	                state.target = newTarget;
	            }, 0);
	        },
	        getTarget: function getTarget() {
	            return state.target;
	        },
	        setIsSent: function setIsSent() {
	            state.isSent = true;
	        },
	        getIsSent: function getIsSent() {
	            return state.isSent;
	        },
	        getCurrentStep: function getCurrentStep() {
	            // Source wasn't picked.
	            if (state.source === undefined) {
	                return { 'state': 'source' };
	            }
	            // Target wasn't picked.
	            else if (state.target === undefined) {
	                    return { 'state': 'target' };
	                }
	                // User isn't finished building the polygon.
	                else if (state.currObstacle.length > 0) {
	                        return { 'state': 'obstacle' };
	                    }
	                    // Grid wasn't sent yet.
	                    else if (state.isSent) {
	                            return { 'state': 'sent' };
	                        }
	                        // Grid can be sent.
	                        else {
	                                return { 'state': 'done' };
	                            }
	        },
	        getGrid: function getGrid() {
	            return state;
	        },
	        /*
	        Grid can be sent if it wasn't already sent,
	        source and target are set and if the user isn't currently
	        working on a new polygon.
	         */
	        canSendGrid: function canSendGrid() {
	            return !state.isSent && state.source !== undefined && state.target !== undefined && state.currObstacle.length === 0;
	        },
	        reset: function reset() {
	            $timeout(function () {
	                state.isSent = false;
	                state.source = undefined;
	                state.target = undefined;
	                state.currObstacle = [];
	                state.obstacles = [];
	            }, 0);
	        }
	    };
	}
	
	logicService.$inject = ['$timeout'];
	exports['default'] = logicService;
	module.exports = exports['default'];

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	function drawService() {
	    "ngInject";
	
	    var stage = new createjs.Stage('grid');
	    var stageStyle = {
	        obstacle: {
	            fill: {
	                color: '#6eb9ff'
	            },
	            stroke: {
	                color: '#2761ff',
	                width: 1
	            },
	            vertex: {
	                color: '#0073ff',
	                radius: 1.5
	            }
	        },
	        path: {
	            color: '#5b0700',
	            width: 1
	        },
	        location: {
	            text: {
	                color: '#ff3800',
	                fontStyle: 'bold 12px Arial',
	                textBaseline: 'hanging'
	            },
	            vertex: {
	                color: '#972300',
	                radius: 2.5
	            }
	        }
	    };
	    return {
	        registerListener: function registerListener(evt, listener) {
	            stage.on(evt, listener);
	        },
	        fillPolygon: function fillPolygon(shape) {
	            var fill = new createjs.Shape();
	            fill.graphics.beginFill(stageStyle.obstacle.fill.color);
	            fill.graphics.moveTo(shape[0]['x'], shape[0]['y']);
	            for (var i = 1; i < shape.length; i++) {
	                fill.graphics.lineTo(shape[i]['x'], shape[i]['y']);
	            }
	            stage.addChild(fill);
	            stage.setChildIndex(fill, 0);
	            stage.update();
	        },
	        addPathToStage: function addPathToStage(path) {
	            var stroke = new createjs.Shape();
	            stroke.graphics.setStrokeStyle(stageStyle.path.width);
	            stroke.graphics.beginStroke(stageStyle.path.color);
	            stroke.graphics.moveTo(path[0]['x'], path[0]['y']);
	            for (var i = 1; i < path.length; i++) {
	                stroke.graphics.lineTo(path[i]['x'], path[i]['y']);
	            }
	            stroke.graphics.endStroke();
	            stage.addChild(stroke);
	            stage.update();
	        },
	        addLineToStage: function addLineToStage(line) {
	            var stroke = new createjs.Shape();
	            stroke.graphics.setStrokeStyle(stageStyle.obstacle.stroke.width);
	            stroke.graphics.beginStroke(stageStyle.obstacle.stroke.color);
	            stroke.graphics.moveTo(line['x1'], line['y1']);
	            stroke.graphics.lineTo(line['x2'], line['y2']);
	            stroke.graphics.endStroke();
	            stage.addChild(stroke);
	            stage.update();
	        },
	        addCircleToStage: function addCircleToStage(point, type) {
	            var circle = new createjs.Shape();
	            circle.graphics.beginFill(stageStyle[type]['vertex'].color).drawCircle(point['x'], point['y'], stageStyle[type]['vertex'].radius);
	            stage.addChild(circle);
	            stage.update();
	        },
	        addLetterToStage: function addLetterToStage(letter, point) {
	            var text = new createjs.Text(letter, stageStyle.location.text.fontStyle, stageStyle.location.text.color);
	            text.x = point['x'] + 2;
	            text.y = point['y'] + 2;
	            text.textBaseline = stageStyle.location.text.textBaseline;
	            stage.addChild(text);
	            stage.update();
	        },
	        reset: function reset() {
	            stage.removeAllChildren();
	            stage.update();
	        }
	    };
	}
	
	exports['default'] = drawService;
	module.exports = exports['default'];

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(1);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _stageComponent = __webpack_require__(9);
	
	var _stageComponent2 = _interopRequireDefault(_stageComponent);
	
	var stageModule = _angular2['default'].module('stage', []).component('stage', _stageComponent2['default']);
	exports['default'] = stageModule;
	module.exports = exports['default'];

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _stageController = __webpack_require__(10);
	
	var _stageController2 = _interopRequireDefault(_stageController);
	
	var _stageTmplHtml = __webpack_require__(11);
	
	var _stageTmplHtml2 = _interopRequireDefault(_stageTmplHtml);
	
	var stageComponent = {
	    restrict: 'E',
	    bindings: {},
	    template: _stageTmplHtml2['default'],
	    controller: _stageController2['default'],
	    controllerAs: 'vm'
	};
	
	exports['default'] = stageComponent;
	module.exports = exports['default'];

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var StageController = function StageController(LogicService, DrawService) {
	    "ngInject";
	
	    _classCallCheck(this, StageController);
	
	    var self = this;
	    self.logSvc = LogicService;
	    self.drawSvc = DrawService;
	    self.drawSvc.registerListener('stagemousedown', handleListener);
	    function handleListener(evt) {
	        if (!self.logSvc.getIsSent()) {
	            handleGridClick(evt.stageX, evt.stageY);
	        }
	    }
	    function handleGridClick(x, y) {
	        // Location of user's click on the canvas.
	        var point = { 'x': x, 'y': y };
	        // Source wasn't set.
	        if (self.logSvc.getSource() === undefined) {
	            self.logSvc.setSource(point);
	            self.drawSvc.addLetterToStage('A', point);
	            self.drawSvc.addCircleToStage(point, 'location');
	        }
	        // Target wasn't set.
	        else if (self.logSvc.getTarget() === undefined) {
	                self.logSvc.setTarget(point);
	                self.drawSvc.addLetterToStage('B', point);
	                self.drawSvc.addCircleToStage(point, 'location');
	            } else {
	                var elem = self.logSvc.addObstacle(point);
	                // Point wasn't ignored.
	                if (elem !== undefined) {
	                    /*
	                    This is the first point of the polygon,
	                    so only draw the point.
	                     */
	                    if (elem.type === 'point') {
	                        self.drawSvc.addCircleToStage(elem.data, 'obstacle');
	                    }
	                    // Also draw a line.
	                    else if (elem.type === 'line') {
	                            if (!elem.hasEnded) {
	                                self.drawSvc.addCircleToStage(point, 'obstacle');
	                            } else {
	                                self.drawSvc.fillPolygon(elem.shape);
	                            }
	                            self.drawSvc.addLineToStage(elem.data);
	                        }
	                }
	            }
	    }
	};
	StageController.$inject = ["LogicService", "DrawService"];
	
	exports['default'] = StageController;
	module.exports = exports['default'];

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	module.exports = "<div class=\"container\">\n    <canvas id=\"grid\" width=\"600\" height=\"400\" class=\"center-block\"></canvas>\n</div>"

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(1);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _operationsComponent = __webpack_require__(13);
	
	var _operationsComponent2 = _interopRequireDefault(_operationsComponent);
	
	var operationsModule = _angular2['default'].module('operations', []).component('operations', _operationsComponent2['default']);
	
	exports['default'] = operationsModule;
	module.exports = exports['default'];

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _operationsController = __webpack_require__(14);
	
	var _operationsController2 = _interopRequireDefault(_operationsController);
	
	var _operationsTmplHtml = __webpack_require__(15);
	
	var _operationsTmplHtml2 = _interopRequireDefault(_operationsTmplHtml);
	
	var operationsComponent = {
	    restrict: 'E',
	    bindings: {},
	    template: _operationsTmplHtml2['default'],
	    controller: _operationsController2['default'],
	    controllerAs: 'vm'
	};
	
	exports['default'] = operationsComponent;
	module.exports = exports['default'];

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var OperationsController = (function () {
	    OperationsController.$inject = ["LogicService", "DrawService", "$http"];
	    function OperationsController(LogicService, DrawService, $http) {
	        "ngInject";
	
	        _classCallCheck(this, OperationsController);
	
	        this.logSvc = LogicService;
	        this.drawSvc = DrawService;
	        this.$http = $http;
	    }
	
	    _createClass(OperationsController, [{
	        key: 'sendGrid',
	        value: function sendGrid() {
	            var self = this;
	            var data = self.logSvc.getGrid();
	            delete data.isSent;
	            delete data.currObstacle;
	            this.$http.post('/api/get_shortest_path', data).success(function (path) {
	                self.drawSvc.addPathToStage(path);
	                self.logSvc.setIsSent();
	            }).error(function (err) {
	                console.error(err);
	            });
	        }
	    }, {
	        key: 'resetGrid',
	        value: function resetGrid() {
	            this.logSvc.reset();
	            this.drawSvc.reset();
	        }
	    }, {
	        key: 'canSendGrid',
	        value: function canSendGrid() {
	            return this.logSvc.canSendGrid();
	        }
	    }]);
	
	    return OperationsController;
	})();
	
	operationsController.$inject = ['LogicService', 'DrawService', '$http'];
	function operationsController(LogicService, DrawService, $http) {
	    return new OperationsController(LogicService, DrawService, $http);
	}
	exports['default'] = operationsController;
	module.exports = exports['default'];

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-offset-4 col-md-2\">\n            <button class=\"btn btn-danger center-block\" type=\"button\" ng-click=\"vm.resetGrid()\">Reset</button>\n        </div>\n        <div class=\"col-md-2\">\n            <button class=\"btn btn-primary center-block\" type=\"button\" ng-click=\"vm.sendGrid()\" ng-disabled=\"!vm.canSendGrid()\">Send</button>\n        </div>\n    </div>\n</div>"

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	  value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _angular = __webpack_require__(1);
	
	var _angular2 = _interopRequireDefault(_angular);
	
	var _messagingComponent = __webpack_require__(17);
	
	var _messagingComponent2 = _interopRequireDefault(_messagingComponent);
	
	var messagingModule = _angular2['default'].module('messaging', []).component('messaging', _messagingComponent2['default']);
	
	exports['default'] = messagingModule;
	module.exports = exports['default'];

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _messagingController = __webpack_require__(18);
	
	var _messagingController2 = _interopRequireDefault(_messagingController);
	
	var _messagingTmplHtml = __webpack_require__(19);
	
	var _messagingTmplHtml2 = _interopRequireDefault(_messagingTmplHtml);
	
	var messagingComponent = {
	    restrict: 'E',
	    bindings: {},
	    template: _messagingTmplHtml2['default'],
	    controller: _messagingController2['default'],
	    controllerAs: 'vm'
	};
	
	exports['default'] = messagingComponent;
	module.exports = exports['default'];

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, '__esModule', {
	    value: true
	});
	
	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }
	
	var MessagingController = (function () {
	    MessagingController.$inject = ["LogicService"];
	    function MessagingController(LogicService) {
	        "ngInject";
	
	        _classCallCheck(this, MessagingController);
	
	        this.logSvc = LogicService;
	        this.stepsText = {
	            'source': 'Click to select a source point',
	            'target': 'Click to select a target point',
	            'obstacle': 'Finish drawing the obstacle',
	            'sent': 'Click reset to start again',
	            'done': 'Start drawing a new obstacle or click send'
	        };
	    }
	
	    _createClass(MessagingController, [{
	        key: 'getMessage',
	        value: function getMessage() {
	            return this.stepsText[this.logSvc.getCurrentStep().state];
	        }
	    }]);
	
	    return MessagingController;
	})();
	
	exports['default'] = MessagingController;
	module.exports = exports['default'];

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	module.exports = "<div class=\"container\">\n  <h5 class=\"text-center center-block msg\">{{ vm.getMessage() }}</h5>\n</div>"

/***/ })
]);
//# sourceMappingURL=app.bundle.js.map