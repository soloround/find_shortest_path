import template from './app.tmpl.html';

let appComponent = {
  template,
  restrict: 'E'
};

export default appComponent;