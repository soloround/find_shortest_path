import controller from './operations.controller';
import template from './operations.tmpl.html';

let operationsComponent = {
    restrict: 'E',
    bindings: { },
    template,
    controller,
    controllerAs: 'vm'
};

export default operationsComponent;