import angular        from 'angular';
import operationsComponent  from './operations.component';

let operationsModule = angular.module('operations', [

])
.component('operations', operationsComponent);

export default operationsModule;