class OperationsController {
    constructor(LogicService, DrawService, $http) {
        "ngInject";

        this.logSvc = LogicService;
        this.drawSvc = DrawService;
        this.$http = $http;
    }
    sendGrid() {
        let self = this;
        let data = self.logSvc.getGrid();
        delete data.isSent;
        delete data.currObstacle;
        this.$http.post('/api/get_shortest_path', data)
            .success(function (path) {
                self.drawSvc.addPathToStage(path);
                self.logSvc.setIsSent();
            })
            .error(function (err) {
                console.error(err);
            });
    }
    resetGrid() {
        this.logSvc.reset();
        this.drawSvc.reset();
    }
    canSendGrid() {
        return this.logSvc.canSendGrid();
    }
}

operationsController.$inject = ['LogicService', 'DrawService', '$http'];
function operationsController(LogicService, DrawService, $http) {
    return new OperationsController(LogicService, DrawService, $http);
}
export default operationsController;