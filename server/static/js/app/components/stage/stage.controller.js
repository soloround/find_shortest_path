class StageController {
    constructor(LogicService, DrawService) {
        "ngInject";

        let self = this;
        self.logSvc = LogicService;
        self.drawSvc = DrawService;
        self.drawSvc.registerListener('stagemousedown', handleListener);
        function handleListener (evt) {
            if (!self.logSvc.getIsSent()) {
                    handleGridClick(evt.stageX, evt.stageY);
            }
        }
        function handleGridClick(x, y) {
            // Location of user's click on the canvas.
            let point = {'x': x, 'y': y};
            // Source wasn't set.
            if (self.logSvc.getSource() === undefined) {
                self.logSvc.setSource(point);
                self.drawSvc.addLetterToStage('A', point);
                self.drawSvc.addCircleToStage(point, 'location');
            }
            // Target wasn't set.
            else if (self.logSvc.getTarget() === undefined) {
                self.logSvc.setTarget(point);
                self.drawSvc.addLetterToStage('B', point);
                self.drawSvc.addCircleToStage(point, 'location');
            }
            else {
                let elem = self.logSvc.addObstacle(point);
                // Point wasn't ignored.
                if (elem !== undefined) {
                    /*
                    This is the first point of the polygon,
                    so only draw the point.
                     */
                    if (elem.type === 'point') {
                        self.drawSvc.addCircleToStage(elem.data, 'obstacle');
                    }
                    // Also draw a line.
                    else if (elem.type === 'line') {
                        if (!elem.hasEnded) {
                            self.drawSvc.addCircleToStage(point, 'obstacle');
                        }
                        else {
                            self.drawSvc.fillPolygon(elem.shape);
                        }
                        self.drawSvc.addLineToStage(elem.data);
                    }
                }
            }
        }
    }
}

export default StageController;