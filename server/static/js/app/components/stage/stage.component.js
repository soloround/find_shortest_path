import controller from './stage.controller';
import template from './stage.tmpl.html';

let stageComponent = {
    restrict: 'E',
    bindings: { },
    template,
    controller,
    controllerAs: 'vm'
};

export default stageComponent;