import angular        from 'angular';
import stageComponent  from './stage.component';

let stageModule = angular.module('stage', [

])
.component('stage', stageComponent);
export default stageModule;