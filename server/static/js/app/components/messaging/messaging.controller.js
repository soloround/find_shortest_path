class MessagingController {
    constructor(LogicService) {
        "ngInject";

        this.logSvc = LogicService;
        this.stepsText = {
            'source': 'Click to select a source point',
            'target': 'Click to select a target point',
            'obstacle': 'Finish drawing the obstacle',
            'sent': 'Click reset to start again',
            'done': 'Start drawing a new obstacle or click send'
        }
    }
    getMessage() {
        return this.stepsText[this.logSvc.getCurrentStep().state];
    }
}

export default MessagingController;