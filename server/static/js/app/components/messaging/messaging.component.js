import controller from './messaging.controller';
import template from './messaging.tmpl.html';

let messagingComponent = {
    restrict: 'E',
    bindings: { },
    template,
    controller,
    controllerAs: 'vm'
};

export default messagingComponent;