import angular        from 'angular';
import messagingComponent  from './messaging.component';

let messagingModule = angular.module('messaging', [

])
.component('messaging', messagingComponent);

export default messagingModule;