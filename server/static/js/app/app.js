import angular       from 'angular';
import uiRouter      from 'angular-ui-router';
import AppComponent  from './app.component';
import LogicService from './services/logicService/logicService';
import DrawService from './services/drawService/drawService';
import StageComponent from './components/stage/stage';
import OperationsComponent from './components/operations/operations';
import MessagingComponent from './components/messaging/messaging';

angular
    .module('app', [
        uiRouter,
        StageComponent.name,
        OperationsComponent.name,
        MessagingComponent.name
    ])
    .config(($locationProvider, $stateProvider, $urlRouterProvider) => {
        "ngInject";

        $stateProvider
            .state('app', {
                url: '/app',
                template: '<app></app>'
            });
        $urlRouterProvider.otherwise('/app');
    })
    .service('LogicService', LogicService)
    .service('DrawService', DrawService)
    .component('app', AppComponent);
