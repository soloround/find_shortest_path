// $timeout is used to invoke the digest cycle,
// so that the view will update.
function logicService($timeout) {
    "ngInject";

    // Radius for determining if two points
    // are too close to each other.
    const radius = 10;
    // Holds current state.
    let state = {
        isSent: false,
        source: undefined,
        target: undefined,
        currObstacle: [],
        obstacles: []
    };
    /*
    Used to lock the polygon and to avoid picking points
    that are too clock to each other.
     */
    function isNearRadius(pointA, pointB) {
        // If point B is within the radius of point A.
        return pointA['x'] - radius <= pointB['x'] &&
                pointB['x'] <= pointA['x'] + radius &&
                pointA['y'] - radius <= pointB['y'] &&
                pointB['y'] <= pointA['y'] + radius;
    }
    // Build a polygon from a list of points.
    function  convertObstacleToPolygon() {
        let polygon = [];
        for(let i = 1; i < state.currObstacle.length; i++) {
            polygon.push(convertPointsToLine(state.currObstacle[i - 1], state.currObstacle[i]));
        }
        return polygon;
    }
    // Converts two points to a line.
    function convertPointsToLine(pointA, pointB) {
        return {
            'x1': pointA['x'],
            'y1': pointA['y'],
            'x2': pointB['x'],
            'y2': pointB['y']
        };
    }
    return {
        addObstacle(point) {
            // First point of the obstacle.
            if (state.currObstacle.length === 0) {
                $timeout(function () {
                    state.currObstacle.push(point);
                }, 0);
                return {
                    'type': 'point',
                    'data': point
                }
            }
            else {
                let firstPoint = state.currObstacle[0];
                let lastPoint = [...state.currObstacle].pop();
                /*
                The polygon consists of at least two points
                and the last point is pretty close to the first point
                of the polygon.
                In this case the polygon is complete.
                */
                if (state.currObstacle.length > 1 && isNearRadius(firstPoint, point)) {
                    let polygonPoints = [...state.currObstacle];
                    polygonPoints.push(firstPoint);
                    $timeout(function () {
                        state.currObstacle.push(firstPoint);
                        let polygonLines = convertObstacleToPolygon();
                        state.currObstacle = [];
                        state.obstacles.push(polygonLines);
                    }, 0);
                    return {
                        'type': 'line',
                        'data': convertPointsToLine(lastPoint, firstPoint),
                        'shape': polygonPoints,
                        'hasEnded': true
                    };
                }
                // The new point isn't too close to the last point entered.
                else if (!isNearRadius(lastPoint, point)) {
                    $timeout(function () {
                            state.currObstacle.push(point);
                    }, 0);
                    return {
                        'type': 'line',
                        'data': convertPointsToLine(lastPoint, point),
                        'hasEnded': false
                    };
                }
                /*
                The new point was too close to the last point entered,
                so it is ignored.
                 */
                else {
                    return undefined;
                }
            }
        },
        setSource(newSource) {
            $timeout(function () {
                state.source = newSource;
            }, 0)
        },
        getSource() {
            return state.source;
        },
        setTarget(newTarget) {
            $timeout(function () {
                state.target = newTarget;
            }, 0)
        },
        getTarget() {
            return state.target;
        },
        setIsSent() {
            state.isSent = true;
        },
        getIsSent() {
            return state.isSent;
        },
        getCurrentStep() {
            // Source wasn't picked.
            if (state.source === undefined) {
                return {'state': 'source'};
            }
            // Target wasn't picked.
            else if (state.target === undefined) {
                return {'state': 'target'};
            }
            // User isn't finished building the polygon.
            else if (state.currObstacle.length > 0) {
                return {'state': 'obstacle'};
            }
            // Grid wasn't sent yet.
            else if (state.isSent) {
                return {'state': 'sent'};
            }
            // Grid can be sent.
            else {
                return {'state': 'done'};
            }
        },
        getGrid() {
            return state;
        },
        /*
        Grid can be sent if it wasn't already sent,
        source and target are set and if the user isn't currently
        working on a new polygon.
         */
        canSendGrid() {
            return !state.isSent &&
                    state.source !== undefined &&
                    state.target !== undefined &&
                    state.currObstacle.length === 0;
        },
        reset() {
            $timeout(function () {
                state.isSent = false;
                state.source = undefined;
                state.target = undefined;
                state.currObstacle = [];
                state.obstacles = [];
            }, 0);
        }
    };
}

logicService.$inject = ['$timeout'];
export default logicService;