function drawService() {
    "ngInject";

    const stage = new createjs.Stage('grid');
    const stageStyle = {
        obstacle: {
            fill: {
                color: '#6eb9ff'
            },
            stroke: {
                color: '#2761ff',
                width: 1
            },
            vertex: {
                color: '#0073ff',
                radius: 1.5
            }
        },
        path: {
            color: '#5b0700',
            width: 1
        },
        location: {
            text: {
                color: '#ff3800',
                fontStyle: 'bold 12px Arial',
                textBaseline: 'hanging'
            },
            vertex: {
                color: '#972300',
                radius: 2.5
            }
        }
    };
    return {
        registerListener(evt, listener) {
            stage.on(evt, listener);
        },
        fillPolygon(shape) {
            let fill = new createjs.Shape();
            fill.graphics.beginFill(stageStyle.obstacle.fill.color);
            fill.graphics.moveTo(shape[0]['x'], shape[0]['y']);
            for (let i = 1; i < shape.length; i++) {
                fill.graphics.lineTo(shape[i]['x'], shape[i]['y']);
            }
            stage.addChild(fill);
            stage.setChildIndex(fill, 0);
            stage.update();
        },
        addPathToStage(path) {
            let stroke = new createjs.Shape();
            stroke.graphics.setStrokeStyle(stageStyle.path.width);
            stroke.graphics.beginStroke(stageStyle.path.color);
            stroke.graphics.moveTo(path[0]['x'], path[0]['y']);
            for (let i = 1; i < path.length; i++) {
                stroke.graphics.lineTo(path[i]['x'], path[i]['y']);
            }
            stroke.graphics.endStroke();
            stage.addChild(stroke);
            stage.update();
        },
        addLineToStage(line) {
            let stroke = new createjs.Shape();
            stroke.graphics.setStrokeStyle(stageStyle.obstacle.stroke.width);
            stroke.graphics.beginStroke(stageStyle.obstacle.stroke.color);
            stroke.graphics.moveTo(line['x1'], line['y1']);
            stroke.graphics.lineTo(line['x2'], line['y2']);
            stroke.graphics.endStroke();
            stage.addChild(stroke);
            stage.update();
        },
        addCircleToStage(point, type) {
            let circle = new createjs.Shape();
            circle.graphics
                .beginFill(stageStyle[type]['vertex'].color)
                .drawCircle(point['x'], point['y'], stageStyle[type]['vertex'].radius);
            stage.addChild(circle);
            stage.update();
        },
        addLetterToStage(letter, point) {
            let text = new createjs.Text(letter, stageStyle.location.text.fontStyle, stageStyle.location.text.color);
            text.x = point['x'] + 2;
            text.y = point['y'] + 2;
            text.textBaseline = stageStyle.location.text.textBaseline;
            stage.addChild(text);
            stage.update();
        },
        reset() {
            stage.removeAllChildren();
            stage.update();
        }
    }
}

export default drawService;