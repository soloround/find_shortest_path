from heapq import heappop, heappush
from collections import defaultdict


class Dijkstra:
    """Dijkstra algorithm for finding the shortest path in a graph.
    See the following for algorithm:
    https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
    """
    def __init__(self, vertices, edges):
        self.vertices = vertices
        self.edges = edges

    def run(self, vertex_source, vertex_target):
        visited = set()
        graph = defaultdict(list)
        queue = [(0, vertex_source, [])]

        # Build graph
        for edge in self.edges:
            cost_delta = edge.calc_len()
            graph[edge.vertex_from].append((edge.vertex_to, cost_delta))
            graph[edge.vertex_to].append((edge.vertex_from, cost_delta))

        # Runs on all vertices
        while queue:
            (cost_from, vertex_from, path) = heappop(queue)
            if vertex_from not in visited:
                visited.add(vertex_from)
                path = path + [vertex_from]
                if vertex_from == vertex_target:
                    return path
                for (vertex_to, cost_delta) in graph.get(vertex_from, ()):
                    if vertex_to not in visited:
                        heappush(queue, (cost_from + cost_delta, vertex_to, path))
        return []

