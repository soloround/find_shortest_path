from flask import Flask, jsonify, request, render_template, send_from_directory
from findshortestpath import FindShortestPath
from os import path

app = Flask(__name__, static_url_path="", static_folder='static')

# Define the host and port.
SERVER_HOST = 'localhost'
SERVER_PORT = 8001


@app.route('/')
def index():
    return render_template('index.tmpl.html')


@app.route('/api/get_shortest_path', methods=['POST'])
def get_shortest_path():
    grid_data = request.get_json()
    shortest_path = FindShortestPath.run(grid_data['source'], grid_data['target'], grid_data['obstacles'])
    return jsonify(shortest_path)


def main():
    app.run(debug=True, host=SERVER_HOST, port=SERVER_PORT)


if __name__ == '__main__':
    main()
