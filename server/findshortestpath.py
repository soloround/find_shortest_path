from buildgraph import BuildGraph
from polygon import Polygon
from vertex import Vertex
from dijkstra import Dijkstra


class FindShortestPath:
    @classmethod
    def run(cls, vertex_source_json, vertex_target_json, polygons_json):
        # Convert json to instances of classes.
        vertex_source = Vertex.from_json(vertex_source_json)
        vertex_target = Vertex.from_json(vertex_target_json)
        polygons = []
        for poly_json in polygons_json:
            polygons.append(Polygon.from_json(poly_json))
        # Run build graph algorithm
        build_graph_algo = BuildGraph(polygons, vertex_source, vertex_target)
        vertices_and_edges = build_graph_algo.run()
        # Run dijkstra algorithm
        dijkstra_algo = Dijkstra(vertices_and_edges[0], vertices_and_edges[1])
        path = dijkstra_algo.run(vertex_source, vertex_target)
        return cls._path_to_json(path)

    @classmethod
    def _path_to_json(cls, path):
        path_json = []
        for vertex in path:
            path_json.append(vertex.to_json())
        return path_json
