from edge import Edge

class Polygon(object):
    def __init__(self, edges):
        self.edges = []
        self.edges += edges

    @classmethod
    def from_json(cls, edges_json):
        edges = []
        for edge_json in edges_json:
            edges.append(Edge.from_json(edge_json))
        return cls(edges)

    def _has_edge(self, edge):
        """Checks if the given edge is one of the polygon's edges"""
        edge_swap = edge.swap_orientation()
        return any(curr_edge == edge or curr_edge == edge_swap for curr_edge in self.edges)

    def _has_vertex(self, vertex):
        """Checks if the given vertex is one of the polygon's vertices"""
        for edge in self.edges:
            if edge.has_vertex(vertex):  # Edge has this vertex as one of its vertices.
                return True
        return False

    def is_diagonal(self, edge):
        """Checks if the given edge is on of polygon's diagonals. """
        vertices = edge.get_vertices()
        if self._has_edge(edge):  # The edge cannot be one of the polygon's edges.
            return False
        else:
            return self._has_vertex(vertices[0]) and self._has_vertex(vertices[1])

    def intersect_with_edge(self, edge):
        """Get all intersection points between the given edge and the polygon.
        Returns intersecting edges' id and their point of intersection.
        """
        intersections = filter(lambda intersection: intersection['point'] is not None,  # None = no intersection.
                                 [{
                                     'type': 'edge',
                                     'edge_id': item[0],
                                     'point': edge.intersect_with_edge(item[1])
                                 } for item in enumerate(self.edges)])
        if len(intersections) > 0:
            # One intersection means that the other intersection point is one of the polygon's vertices.
            if len(intersections) == 1:
                edge_vertices = edge.get_vertices()
                # Find the shared vertex between the line and the polygon.
                vertex_intersect = next((vertex for vertex in edge_vertices if self._has_vertex(vertex)), None)
                if vertex_intersect is not None:
                    intersections.append({
                        'type': 'vertex',
                        'point': vertex_intersect
                    })
            return intersections
        else:
            return None

    def get_adjacent_vertices(self, vertex):
        """Returns all the vertices that are adjacent to the given vertex on this polygon.
        If none found, returns None.
        """
        vertices_adj = []
        for edge in self.edges:
            if edge.has_vertex(vertex):  # Edge has this vertex as one of its vertices.
                edge_vertices = edge.get_vertices()
                # Get the other vertex of the edge.
                vertices_adj.append(edge_vertices[0] if edge_vertices[0] != vertex else edge_vertices[1])
        if len(vertices_adj) > 0:
            return vertices_adj
        else:
            return None
