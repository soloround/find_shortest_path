from math import sqrt, pow


class Vertex(object):
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __ne__(self, other):
        return self.x != other.x or self.y != other.y

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
               self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))

    def calc_dist(self, vertex):
        """Calculates distance between two vertices."""
        return sqrt(pow(self.x - vertex.x, 2) +
                    pow(self.y - vertex.y, 2))

    @classmethod
    def from_json(cls, vertex_json):
        x = vertex_json['x']
        y = vertex_json['y']
        return cls(x, y)

    def to_json(self):
        return {
            'x': self.x,
            'y': self.y
        }
