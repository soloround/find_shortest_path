from edge import Edge


class BuildGraph:
    def __init__(self, polygons, vertex_source, vertex_target):
        self.polygons = polygons
        self.vertex_source = vertex_source
        self.vertex_target = vertex_target

    def run(self):
        """Finds all possible vertices and all valid edges in the path from source to target.
        New vertices are discovered by trying to find new paths between vertices that were already found.
        The algorithm ends when no new vertices can be found.
        """
        vertices_curr = {self.vertex_source, self.vertex_target}
        vertices_prev = set()
        edges = set()
        # Runs until no new vertices are found and added.
        while vertices_curr != vertices_prev:
            vertices_new = set(vertices_curr)
            vertices_curr_enumerable = list(vertices_curr)  # For iterating the vertices.
            # Run on all possible paths between the vertices
            # that were found by now and find new ones.
            for i in range(len(vertices_curr)):
                for j in range(i + 1, len(vertices_curr)):
                    vertex_source = vertices_curr_enumerable[i]
                    vertex_target = vertices_curr_enumerable[j]
                    edge_opt = Edge.from_vertices(vertex_source, vertex_target)
                    # If vertices already have an edge in edges set,
                    # then the path between them is already valid.
                    # This means that no new vertices can be found in that path.
                    if edge_opt not in edges:
                        vertices_or_edges = self._find_new_vertices_or_edges(vertex_source, vertex_target)
                        vertices_new |= vertices_or_edges[0]
                        edges |= vertices_or_edges[1]
            vertices_prev = vertices_curr
            vertices_curr = vertices_new
        return vertices_curr, edges

    def _is_diagonal_in_polygon(self, edge):
        for poly in self.polygons:
            if poly.is_diagonal(edge):
                return True
        return False

    def _find_new_vertices_or_edges(self, vertex_source, vertex_target):
        """Finds intersections with polygons in path from source to target.
        If intersections are found, returns new vertices to avoid the intersection.
        If not, returns new valid edges from source to target.
        """
        vertices = set()
        edges = set()
        optimal_edge = Edge.from_vertices(vertex_source, vertex_target)
        polygons_intersect = self._find_intersections_with_polygons(optimal_edge)
        # The intersections are sorted so that they will be processed in the right order -
        # from source to target.
        for poly_intersect in polygons_intersect:
            self._sort_polygon_intersections(poly_intersect, vertex_source, vertex_target)
        # Path from source to target is obstructed.
        if len(polygons_intersect) > 0:
            polygon_intersect = polygons_intersect[0]
            intersections = polygon_intersect['intersections']
            polygon_id = polygon_intersect['polygon_id']
            polygon = self.polygons[polygon_id]
            intersection = intersections[0]
            # Intersection was with polygon's edge.
            if intersection['type'] == 'edge':
                edge_curr_id = intersection['edge_id']
                edge_curr = polygon.edges[edge_curr_id]
                edge_curr_vertices = edge_curr.get_vertices()
                vertices_possible = [vertex for vertex in edge_curr_vertices
                                     if vertex != vertex_source and vertex != vertex_target]
            # Intersection was with polygon's vertex.
            else:
                vertex_intersection = intersection['point']
                vertices_possible = polygon.get_adjacent_vertices(vertex_intersection)
            if vertices_possible is not None:
                vertices = set(vertices_possible)
        # Path is clear, so we can add an edge from source to target
        # as a valid edge.
        else:
            edge = Edge.from_vertices(vertex_source, vertex_target)
            if self._is_diagonal_in_polygon(edge) is not True:
                edge_swap = edge.swap_orientation()
                edges.add(edge)
                edges.add(edge_swap)
        return vertices, edges

    def _find_intersections_with_polygons(self, edge):
        """Find whether polygon's edges intersect with the given edge.
        If so, return intersecting edges' ids and their intersection point along with the polygon's id.
        """
        polygons_intersect = []
        for item in enumerate(self.polygons):
            edges_intersect = item[1].intersect_with_edge(edge)
            if edges_intersect is not None:
                polygons_intersect.append({
                    'polygon_id': item[0],
                    'intersections': edges_intersect
                })
        return polygons_intersect

    def _sort_polygon_intersections(self, intersect_poly, vertex_source, vertex_target):
        """Sorts intersections so that they will be ordered from source vertex to target vertex."""
        # y_direction and x_direction are used to find out the order in which the intersections should be processed.
        # The order is set by the locations of the source and target vertices.
        # The order should be from the source to the target upon traversing the polygons.
        y_direction = 1 if vertex_source.y < vertex_target.y else \
            0 if vertex_source.y == vertex_target.y else -1
        x_direction = 1 if vertex_source.x < vertex_target.x else -1
        if y_direction != 0:
            intersect_poly['intersections'].sort(key=lambda intersect: intersect['point'].y,
                                                 reverse=True if y_direction < 0 else False)
        else:
            intersect_poly['intersections'].sort(key=lambda intersect: intersect['point'].x,
                                                 reverse=True if x_direction < 0 else False)
        return intersect_poly
